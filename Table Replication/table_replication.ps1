# Table replication
# v 0.9
$tenantId = "<tenant_id>"
$subscriptionId = "<subscription_id>"
$resourceGroupName = "<resource_group_name>"
$workspaceName = "<workspace_name>"
$tableName = "<source_table>"
$newTableName = "<destination_table>"
$plan = "<table plan>" # analytics / basic / auxiliary
$retentionInDays = -1
$totalRetentionInDays = -1

# Aux: retention: -1, totalretention: 365
# Basic: -1, -1
# Analytics: your choice


$endpoint = "https://management.azure.com/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.OperationalInsights/workspaces/$workspaceName/tables/$tableName`?api-version=2023-09-01"
$newTableEndpoint = "https://management.azure.com/subscriptions/$subscriptionId/resourceGroups/$resourceGroupName/providers/Microsoft.OperationalInsights/workspaces/$workspaceName/tables/$newTableName`?api-version=2023-01-01-preview"



function Get-Token{
    #Getting Azure Access Token for authentication
    param ([string]$tenantId )

    $null = Connect-AzAccount -Tenant $TenantID -UseDeviceAuthentication
    return (Get-AzAccessToken).Token 

}

function GetAndSanitize-TableColumns{
    #Getting the columns of the original table and sanitizing the values
    param ([string]$endpoint,
        [string]$AzureAccessToken)

    $excludedColumns = @("TenantId") # some default columns cannot be manually created

    # Get the table schema
    $response = Invoke-RestMethod -Uri $endpoint -Method Get -Headers @{
        "Authorization" = "Bearer $AzureAccessToken"
    }

    # Extract columns and standardColumns
    $columns = $response.properties.schema.columns
    $standardColumns = $response.properties.schema.standardColumns

    # Combine columns and standardColumns
    $newColumns = $columns + $standardColumns

    # Remove excluded columns
    $newColumns = $newColumns | Where-Object { $excludedColumns -notcontains $_.name }

    # Replace “ and ” with ' in column names
    # some characters are working just fine in Azure Cloud, but not in on-prem PS, so I'm replacing them in the description field
    foreach ($column in $newColumns) {
        $column.description = $column.description.Replace([char]0x201C, "'").Replace([char]0x201D, "'")
    }

    return $newColumns

}

function Create-Table{
    param ([string]$newTableEndpoint,
    [string]$AzureAccessToken,
    [string]$newTableName,
    [array]$newColumns,
    [int]$retentionInDays,
    [int]$totalRetentionInDays,
    [string]$plan
    )
    # Define the request body for creating the new table
    $requestBody = @{
        properties = @{
            schema = @{
                name = $newTableName
                columns = $newColumns
            }
            retentionInDays = $retentionInDays
            totalRetentionInDays = $totalRetentionInDays
            plan = $plan
        }
    }

    # Convert the request body to JSON
    $jsonRequestBody = $requestBody | ConvertTo-Json -Depth 5


    # Create the new table
    try {
        Invoke-RestMethod -Uri $newTableEndpoint -Method Put -Headers @{
            "Authorization" = "Bearer $AzureAccessToken"
            "Content-Type" = "application/json"
        } -Body $jsonRequestBody
        Write-Output "New table $newTableName created successfully."
    } catch {
        Write-Error "Failed to create the new table $newTableName. Error: $_"
    }



}


function Main {

    # Connect to Azure
    $AzureAccessToken =  Get-Token -tenantId  $tenantId 

    #Get Columns
    $newColumns =  GetAndSanitize-TableColumns -endpoint $endpoint -AzureAccessToken $AzureAccessToken

    #Create new table
    Create-Table -newTableEndpoint $newTableEndpoint -AzureAccessToken $AzureAccessToken -newTableName $newTableName -newColumns $newColumns -retentionInDays $retentionInDays -totalRetentionInDays $totalRetentionInDays -plan $plan

}

Main