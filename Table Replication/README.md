# Table replication

The code can be used to copy a table schema and create a new Analytics, Basic or Auxiliary tables.
Version 0.9 (under testing)

## Parameters
Modify the code and provide the following parameters

- $tenantId = "<tenant_id>"
- $subscriptionId = "<subscription_id>"
- $resourceGroupName = "<resource_group_name>"
- $workspaceName = "<workspace_name>"
- $tableName = "<source_table>" --> the table you want to copy
- $newTableName = "<destination_table>" --> the table you want to create
- $plan = "<table plan>" # analytics / basic / auxiliary
- $retentionInDays = -1
- $totalRetentionInDays = -1

The authentication happens via Device Authentication.

Retention recommendations:
- Analytics table: -
- Basic: retention: -1 (otherwise it will fail), totalretention: up to you
- Auxiliary: retention: -1(otherwise it will fail), totalretention: 365 (otherwise it will fail)

![Table plan and retention info](https://learn.microsoft.com/en-us/azure/azure-monitor/logs/media/manage-logs-tables/azure-monitor-logs-table-management.png)

## Usage
This code allows you to create new tables based on an existing one. I use this script to split logs into separate tables. 

> Note that it won't create any Data Collection Endpoint (DCE) or Data Collection Rule (DCR). / Although log splitting doesn't work for Auxiliary (AUX) tables, you can still use the code to create an AUX table.


### Sample Usage
Suppose you have three different log sources in the CommonSecurityLog table: IBM, Cisco, and Palo Alto Networks logs. You want to set different Role-Based Access Control (RBAC) or retention policies for these logs, or perhaps you simply want better query performance. In that case, you can store them in separate tables using the code:

So, you can go ahead and create 3 new tables using my code
1. Create an Analytics table for IBM logs with the CommonSecurityLog schema.
2. Create an Analytics table for Palo Alto Networks logs with the same schema.
3. Create a Basic table for Cisco logs, considering them less critical.


Next, navigate to the native CommonSecurityLog DCR that already collects these logs and configure the log splitting logic accordingly:

```json
"dataFlows": [
  {
    "streams": [
      "Microsoft-CommonSecurityLog"
    ],
    "destinations": [
      "DataCollectionEvent"
    ],
    "transformKQL": "source | where DeviceVendor !in ('Cisco','Palo Alto Networks','IBM')"
  },
  {
    "streams": [
      "Microsoft-CommonSecurityLog"
    ],
    "destinations": [
      "DataCollectionEvent"
    ],
    "transformKQL": "source | where DeviceVendor == 'Cisco'",
    "outputStream": "Custom-CSL_Cisco_CL"
  },
  {
    "streams": [
      "Microsoft-CommonSecurityLog"
    ],
    "destinations": [
      "DataCollectionEvent"
    ],
    "transformKQL": "source | where DeviceVendor == 'IBM'",
    "outputStream": "Custom-CSL_IBM_CL"
  },
  {
    "streams": [
      "Microsoft-CommonSecurityLog"
    ],
    "destinations": [
      "DataCollectionEvent"
    ],
    "transformKQL": "source | where DeviceVendor == 'Palo Alto Networks'",
    "outputStream": "Custom-CSL_Palo_CL"
  }
]
```