# SentinelLAWBilling query

## In progress, the code is not finished yet!

The query calculates the price of Sentinel and Log Analytics Workspace usage based on the ingested data. The query is not there to give a precise estimation. I put it together to help you choose which Commitment Tier you should choose based on your historical data to get the cheapest option.

## Design considerations

### The ratio between Sentinel and Law pricing
SentinelPAYGGbPrice constant is used to define the Pay-As-You-Go price of data ingested into Sentinel per Gb. This price depends on the location of your Log Analytics Workspace. <br/>
LAWPAYGGbPrice constant is used to define the PAYG price of data ingested into your Log Analytics Workspace per Gb. This price depends on the location of your Log Analytics Workspace. <br/>
Normally the difference ratio of the Pay-As-You-Go price of Sentinel and LAW (Log Analytics Workspace) is x1.15. So LAW is 1.15times more expensive than Sentinel. This is only an estimation though because Microsoft rounds the numbers here and there. <br/>
If you don't have the exact prices, or you don't want to use them, you can use the pre-defined 1 and 1.15. Using the query with these values tells you which commitment tier to choose but not going to tell you the exact cost. </br>

*This 15% is not true in every situation. Some location can have different ratio. I modified the code, so now you can provide the exact pricing of the different Commitment Tiers. This way the calculation will work in case of any locations.*

### Commitment Tier pricing calculation
I decided to calculate the price of the Commitment Tiers (the fixed part of the cost) with this equation: 
Price = {CommitmentTierAmount} * ( 1 - {Savings})  

For example, the 300Gb Tier in Sentinel in West US 2 (in EUR) has the following data:
- Data = 300Gb
- Price = 220Eur
- Effective Price per GB = 0.74 EUR/GB
- Saving over Pay-As-You-Go = 57%

My calculation:
300Gb * (1 - 0.57)GB/EUR = 129
This is a ratio, and we will get the exact number if we multiply this with the Pay-As-You-Go price of 1.69. After this, the price we get is 218.1.
So the number is not the 220 that was defined by MS, but it is close, so it is good enough for our calculation.

*This calculation is not true anymore. Even though the portal: https://azure.microsoft.com/en-us/pricing/details/monitor/ states that the discount percentages are fix between locations this is actually not true. So I'm using exact prices instead of discounts. Thus, the price is calcualted as {CommitmentTierAmount} * {Effective_Price}.*  

### let
I defined a lot of different code snippets with the 'let' function, so it is easier to re-use any part of the code if needed.

### Same commitment tiers
You can choose different commitment tiers for LAW and Sentinel, you don't have to use the same configuration. This is really important because the discount they give you is different. The 100Gb Tier in case of Sentinel gives you a 50% discount, while in the case of LAW it only gives you 15%. <br/>
Because of this, it can be beneficial to pick different commitment tiers for Sentinel and LAW.

**Example calculation:** North Europe in EUR for 100GB Tier for Sentinel:
- Pay-As-You-Go price:2.03 EUR/GB
- Price of the tier: 102 EUR/day and includes 100GB
- Effective Per GB Price: 1.02 EUR/GB
- Savings over Pay-As-You-Go: 50% (not really, 2.03/1.02 is not 50% but close, MS rounds the numbers)

So pushing 50GB of data/day Pay-As-You-Go is: 50GB x 2.03EUR/GB = 101.5EUR <br/>
With the Commitment Tier 100 this is a fix: 102EUR <br/>
They are practically the same. But until you reach 100GB you won't pay anything else in case of Tier100, while you will pay an extra 2.03EUR/GB in case of Pay-As-You-Go. </br>
**So if you push 50GB per day into Sentinel reliably it is worth setting the Tier to a higher level (from PAYG to T100).**

The same calculation for LAW:
- PAYG price: 2.328 EUR/GB
- Price of the Tier: 198.35 EUR/day and includes 100GB
- Effective Per GB Price: 1.99 EUR/GB
- Savings: 15% (14.5% in reality based on the numbers)

Pushing ~85GB data per day into Sentinel PAYG: 85GB x 2.328EUR/GB = 197.88 <br/>
With the Commitment Tier 100, this is a fix: 198.35 EUR </br>
Again, these numbers are almost the same. If you push in one additional GB of data then PAYG becomes more expensive for that day.

So which Tiers you should choose in a situation like this?
- Sentinel: From this calculation, it is clear that above 50GB it is worth choosing Tier100 instead of going with the Pay-As-You-Go. This is really important to see because I have seen companies only changed the tiering when they reached the amount of the given tier. Like changing to Tier100 when they have more than 100GB ingested data.
- LAW: On the other hand, you should only change LAW when to T100 when you reached 85GB of ingested data per day. So, hopefully, it is clear now, that the two settings can and have to be different from time to time.

To provide a good suggestion, my query provides you the best cost (and Tier) for Sentinel and LAW separately.

## Free 5GB
Every first 5GB/month data ingested into LAW is free (you still have to pay after Sentinel). I left this free amount out of my calculations. This amount is so low it won't actively affect the results. When we are talking about Commitment Tiers the first amount where it is relevant is the 50GB/day ingestion (based on the previous calculation). 5 GB/month is approximately 170MB/day, and this amount is insignificant when compared to 50GB/day.

## Manual 

Constants:
- starttime: Define the start time of the calculation. Logs, starting from this day are going to be used to calculate the best commitment tier.
- endtime: Define the end time of the calculation. Due to the 'startofday' function if you use today as an end date the logs arrived today are not going to be counted. This is there to eliminate half days (today is still ongoing) in the calculation.
- SentinelPAYGGbPrice: Pay-As-You-Go price of Sentinel at the used location. If you don't want to use the exact cost information you can leave the '1' here and '1.15' in the next field.
- LAWPAYGGbPrice: Pay-As-You-Go price of Sentinel at the used location. If you don't want to use the exact cost information you can leave the '1' in the previous field and '1.15' here.
- percentageIncrease: You can define an estimated percentage increase of the ingested data. By default, it is 0, which means it calculates the best Tiering based on your historical data. If you configure this number then the query calculates the best tiering if the ingested data had been X% higher.
- overusage: A function to calculate the used data above the reserved one in case of commitment tiers.
- MBPerDay: Calculates the used amount of Data (in MB) per day.
- SentinelPricing: Calculates the costs of the various Commitment Tiers for Sentinel.
- LAWPricing: Calculates the costs of the various Commitment Tiers for Sentinel.
 
## Evaluation of the results

After executing pricing_per_service you will get a similar output:
![KQL output](./static/evaluation_of_results.PNG)
The first column defines whether the value is realted to Sentinel pricing or Log Analytics Workspace pricing. <br/>
The second column tells us what could have been our cheapest option over the defined period of time. Knowing this value we just have to find the related Commitment tier. <br/>
In my example, the lowest cost for LAW is 17,559.519. One can find the same value in the Commitment Tier 200 column, so if we had been commited to 200GB/day then we would have got the lowest cost. <br/>
For Sentinel, the lowest possible cost according to the calculator is 8,531.8. This cost is associated with the 300GB/day Commitment Tier. So it is clear from this, that choosing different Commitment Tiers for LAW and Sentinel would have been beneficial for us.


## Notification
Please be aware, to simplify the calculations and make them future proof I decided to use estimations instead of using the fix, provided prices for each step by Microsoft.
Because of this, the calculations are not going to provide you the exact costs they only provide a good estimation. Thus, this is not a price calculator, this is only a tool to help you choose the proper commitment tier.
