# Rule and Parser Converter

## Description
These scripts convert rule and parser (savedSearch) JSON files to deployable ARM (Azure Resource Manager) JSON files. It is designed to streamline the deployment process by converting the rule definitions into a format suitable for ARM templates.

## Prerequisites
Ensure you have the necessary permissions and the following software installed:
- PowerShell on-prem
- PowerShell in Azure

## Note
The files in the official Microsoft Sentinel repository are in YAML format. Therefore, you need to convert the YAML files to JSON before using this script.
To do this you can use the powershell-yaml module.


## Usage

### Sample Command
```powershell
.\jsonrule2arm.ps1 -inputFilePath .\rule01.json -outputFilePath .\rule01.arm.json
