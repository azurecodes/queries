Param(
    [string] [Parameter(Mandatory = $true)] $inputFilePath,
    [string] [Parameter(Mandatory = $true)] $outputFilePath
)

#$inputFilePath = "C:\Users\tokes\Downloads\jsonrule_raw.json"
#$outputFilePath = "C:\Users\tokes\Downloads\jsonrule_raw_v2.json"



# Define the compare operator mapping
$CompareOperatorYaml2Arm = @{
    "eq" = "Equals"
    "gt" = "GreaterThan"
    "ge" = "GreaterThanOrEqual"
    "lt" = "LessThan"
    "le" = "LessThanOrEqual"
}

# Define the required parameters
$RequiredParameters = @{
    "suppressionDuration" = "PT1H"
    "suppressionEnabled"  = $false
    "enabled"             = $true
    "customDetails"       = $null
    "entityMappings"      = $null
    "templateVersion"     = "1.0.0"
}

# Read the input JSON file
$inputJson = Get-Content -Path $inputFilePath -Raw | ConvertFrom-Json

# Create the ARM template structure
$template = @{
    '$schema' = "https://schema.management.azure.com/schemas/2019-04-01/deploymentTemplate.json#"
    contentVersion = "1.0.0.0"
    parameters = @{
        workspace = @{
            type = "String"
        }
        "analytic-id" = @{
            type = "string"
            defaultValue = $inputJson.id
            minLength = 1
            metadata = @{
                description = "Unique id for the scheduled alert rule"
            }
        }
    }
    resources = @(
        @{
            type = "Microsoft.OperationalInsights/workspaces/providers/alertRules"
            name = "[concat(parameters('workspace'),'/Microsoft.SecurityInsights/',parameters('analytic-id'))]"
            apiVersion = "2020-01-01"
            kind = $inputJson.kind
            location = "[resourceGroup().location]"
            properties = @{}
        }
    )
}

# Copy all properties from input JSON to ARM template properties, excluding specific fields
$excludeFields = @("id", "kind", "name", "version")
$inputJson.PSObject.Properties | ForEach-Object {
    $value = $_.Value
    if ($CompareOperatorYaml2Arm.ContainsKey($value)) {
        $value = $CompareOperatorYaml2Arm[$value]
    }
    if ($excludeFields -notcontains $_.Name) {
        $template.resources[0].properties.Add($_.Name, $value)
    }
    # Translate 'version' to 'templateVersion'
    if ($_.Name -eq "version") {
        $template.resources[0].properties.Add("templateVersion", $value)
    }
    if ($_.Name -eq "name") {
        $template.resources[0].properties.Add("displayName", $value)
    }
}


# Add required parameters if they don't exist, except 'templateVersion' if already added
$RequiredParameters.GetEnumerator() | ForEach-Object {
    if (-not $template.resources[0].properties.ContainsKey($_.Key)) {
        $template.resources[0].properties.Add($_.Key, $_.Value)
    }
}

# Convert the template to JSON format
$templateJson = $template | ConvertTo-Json -Depth 10

# Process the JSON string with the provided command
$templateJson = $templateJson -replace '"([0-9]+)m"', '"PT$1M"' -replace '"([0-9]+)h"', '"PT$1H"' -replace '"([0-9]+)d"', '"P$1D"'
$templateJson = $templateJson -replace "\\u0027", "'"
# Save the processed JSON to the output file
Set-Content -Path $outputFilePath -Value $templateJson

Write-Host "Conversion complete. The ARM template has been saved to $outputFilePath."
