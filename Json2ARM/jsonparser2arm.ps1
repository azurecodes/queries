Param(
    [string] [Parameter(Mandatory = $true)] $inputFilePath,
    [string] [Parameter(Mandatory = $true)] $outputFilePath
)

# Read the input JSON file
$inputJson = Get-Content -Path $inputFilePath -Raw | ConvertFrom-Json

# Create the ARM template structure
$template = @{
    '$schema' = "https://schema.management.azure.com/schemas/2019-08-01/deploymentTemplate.json#"
    contentVersion = "1.0.0.0"
    parameters = @{
        Workspace = @{
            type = "string"
            metadata = @{
                description = "The Microsoft Sentinel workspace into which the function will be deployed. Has to be in the selected Resource Group."
            }
        }
        WorkspaceRegion = @{
            type = "string"
            defaultValue = "[resourceGroup().location]"
            metadata = @{
                description = "The region of the selected workspace. The default value will use the Region selection above."
            }
        }
    }
    resources = @(
        @{
            type = "Microsoft.OperationalInsights/workspaces/savedSearches"
            apiVersion = "2024-03-01"
            name = "[concat(parameters('Workspace'), '/', `'$($inputJson.FunctionAlias)`')]"
            location = "[parameters('WorkspaceRegion')]"
            properties = @{
                etag = "*"
                displayName = $inputJson.Function.Title
                category = $inputJson.Category
                FunctionAlias = $inputJson.FunctionAlias
                query = $inputJson.FunctionQuery
                #version = $inputJson.Function.Version #query language version is not stored
                functionParameters = $inputJson.FunctionParameters
                tags = $inputJson.tags
            }
        }
    )
}

# Convert the template to JSON format and save it to the output file
$templateJson = $template | ConvertTo-Json -Depth 10 | Out-String
$templateJson = $templateJson -replace "\\u0027", "'"

Set-Content -Path $outputFilePath -Value $templateJson

Write-Host "Conversion complete. The ARM template has been saved to $outputFilePath."
