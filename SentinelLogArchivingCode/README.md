Microsoft made the new archiving feature available in Sentinel not that long time ago.

Articles about it:
* https://techcommunity.microsoft.com/t5/microsoft-sentinel-blog/ingest-archive-search-and-restore-data-in-microsoft-sentinel/ba-p/3195126
* https://techcommunity.microsoft.com/t5/microsoft-sentinel-blog/faq-search-basic-ingestion-archive-and-data-restoration/ba-p/3205600

MS provides two methods (codes) to enable archiving:
1. With a workbook: The drawback of the workbook is that you can only configure tables which contains logs (it is based on the Usage table) and you can only configure the tables one-by-one.
2. They also provided a PowerShell code that can be used to configure retention via a PS GUI: There are some smaller bugs in the code so the configuration is not working all the time.

To solve these issues I created a PS script based on Microsoft's PS code. 

About my code:
* My script can be executed from a local PowerShell or from Cloud shell in Azure.
* The code uses DeviceAuthentication.
* Can be used to configure all existing table in Sentinel or to define a list of tables.
* To use a pre-defined list of tables modify the ConfigureTables variable (array) at the beginning of the code.
* If you just copy the code and execute it, it will ask you to authenticate (via Device Authentication) and then it will go ahead and configure all the existing tables.
* The code only configures the 'Analytics' tables and not the 'Basic' tables.

Parameters:
* SubscriptionId
* LogAnalyticsResourceGroup
* LogAnalyticsWorkspaceName
* TenantID : for Authentication
* TotalRetentionInDays: Configure the days for which you want to keep the logs. The TotalRetentionInDays value minus the already configured Retention is going to be the archieve retention.
* ConfigureTables: If you don't want to configure all the tables you can define a list in this variable. If you want to configure every table then leave the variable intact. 

Example: table definition:
```powershell
$ConfigureTables = [System.Collections.ArrayList]@('BehaviorAnalytics','Zoomer_CL')
```
