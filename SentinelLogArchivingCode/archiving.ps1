# parameters
$SubscriptionId = "<id>"
$LogAnalyticsResourceGroup = "<rg_name>"
$LogAnalyticsWorkspaceName = "<ws_name>"
$TenantID = "<tenant_id>"
$TotalRetentionInDays = 90 #TotalRetentionInDays - the already configured retention period for the table = archive period 
$ConfigureTables = [System.Collections.ArrayList]@() # if you define a list of table names in this variable then only those tables are going to be configured

# Connect to Azure
Connect-AzAccount -Tenant $TenantID -UseDeviceAuthentication

Set-AzContext -SubscriptionID $SubscriptionId
 
$AzureAccessToken = (Get-AzAccessToken).Token            
$LaAPIHeaders = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
$LaAPIHeaders.Add("Content-Type", "application/json")
$LaAPIHeaders.Add("Authorization", "Bearer $AzureAccessToken")


# Collect all the existing Analytics (not Basic) tables
if (-Not ($ConfigureTables.count -gt 0)){
	$ConfigureTables = [System.Collections.ArrayList]@() 
	$TablesApi = "https://management.azure.com/subscriptions/$SubscriptionId/resourcegroups/$LogAnalyticsResourceGroup/providers/Microsoft.OperationalInsights/workspaces/$LogAnalyticsWorkspaceName/tables" + "?api-version=2021-12-01-preview"
	$TablesApiResult = Invoke-RestMethod -Uri $TablesApi -Method "GET" -Headers $LaAPIHeaders 
 
	foreach ($ta in $TablesApiResult.value){
			if ($ta.properties.Plan -eq 'Analytics'){
				# Processing only the Analytics logs (not the Basic ones)
				$null = $ConfigureTables.Add($ta.name.Trim());
			}
		}
}

# Create a list of tables
# If you want to use pre-defined tables instead of the ones the script found you can just overwrite the ConfigureTables variable

# Configuring the tables one by one
foreach ($tbl in $ConfigureTables)  {
		$TablesApi = "https://management.azure.com/subscriptions/$SubscriptionId/resourcegroups/$LogAnalyticsResourceGroup/providers/Microsoft.OperationalInsights/workspaces/$LogAnalyticsWorkspaceName/tables/$($tbl)" + "?api-version=2021-12-01-preview"		
        $ArchiveDays = [int]($TotalRetentionInDays)
        
        $TablesApiBody = @"
			{
				"properties": {
					"retentionInDays": null,
					"totalRetentionInDays":$ArchiveDays
				}
			}
		"@
		
		try {        
			$TablesApiResult = Invoke-RestMethod -Uri $TablesApi -Method "PUT" -Headers $LaAPIHeaders -Body $TablesApiBody	
			Write-Host "$($tbl) DONE - TotalRetentionInDays: $($ArchiveDays) days"		
		} 
		catch {                    
			Write-Host -Message "Update-TablesRetention $($_) for table: $($tbl)"                 
		}
	}

